#!/usr/bin/env python

# This script extracts settings from the gcode comments shown below.
# The gcode comments should be added to the filament settings start G-Code in Slic3r
# Each layer should begin with
# ;LAYER:[layer_num]   - where [layer_num] is replaced by the layer number
#
# example Usage: 
#           ./setLayerTemperatur.py -s 215 -e 260 -t 5 -f tempTower_PETG2.gcode
#   or:     ./setLayerTemperatur.py --startTemp=215 --endTemp=260 --tempStep=5 --gcodeFile=tempTower_PETG2.gcode
#

import sys, re, decimal #argpar se


# parser = argparse.ArgumentParser(description="Sets the proper temperatures to the corresponding layers of a gcode file exported from Slic3r. This allows the temperature tower to have different temperatures per block.")
# requiredNamed = parser.add_argument_group('required arguments')
# requiredNamed.add_argument('-i', '--input', help="The gcode file to process", required = True)
# # requiredNamed.add_argument('-o'), '--output', help="Resultant gcode file", required = True)
# args = parser.parse_args()

layer_height = .2
base_height = 1.2
floor_height = 5 #TODO adjust this to match .2 layer height
num_floors = 18
layer_tag = ";LAYER:"


base_layers = base_height/ layer_height
floor_layers = floor_height / layer_height
input_file_retraction_setting = 2


def main():

    input_file_path = 'res/triangle_tower_2_rl.gcode'
    output_file_path = 'output/triangle_tower_progressive.gcode'
    input_file_retraction = 2
    current_layer = 0
    current_retraction = 0
    current_retraction_speed = 0

    layer_list = generate_floor_layers()
    
    output_file = open(output_file_path, 'w')
    with open(input_file_path) as input_file:
        for line in input_file:
            if(line.find('before_layer_gcode') != -1):
                print('Reached end of Gcode')
                break
            if(line.find(layer_tag) != -1):
                current_layer = int(line.split(';LAYER:',1)[1].rstrip())
                # if(current_layer == 133):
                #     break
                if(current_layer in layer_list):
                    current_retraction += .5
                    print("Layer " + str(current_layer) + " increasing current retraction to:" + str(current_retraction))            
            if(is_movement_code(line) and current_layer >= base_height / layer_height):
                new_code = adjust_extrude_amount(line, current_retraction - input_file_retraction)
                output_file.write(str(new_code))
            elif(is_extrude_only_code(line) and current_layer >= base_height / layer_height):
                if(is_default_extrude(line)):
                    new_code = adjust_extrude_amount(line, current_retraction - input_file_retraction)
                    output_file.write(str(new_code))
                else:
                    if(current_layer >= 33): #Skip copying retract line if current retraction is 0
                        output_file.write(line)
            else:
                output_file.write(line)

def adjust_ret_speed():
    input_file_path = 'res/retraction_speed_40.gcode'
    output_file_path = 'output/OUT_triangle_tower_rets.gcode'    
    current_layer = 0
    current_retraction_speed = 10
    retraction_speed_increment = 5 #mm/s

    layer_list = generate_floor_layers()
    
    output_file = open(output_file_path, 'w')
    with open(input_file_path) as input_file:
        for line in input_file:
            if(line.find('before_layer_gcode') != -1):
                print('Reached end of Gcode')
                break
            if(line.find(layer_tag) != -1):
                current_layer = int(line.split(';LAYER:',1)[1].rstrip())
                if(current_layer in layer_list):
                    current_retraction_speed += retraction_speed_increment
                    print("Layer " + str(current_layer) + " increasing current retraction speed to:" + str(current_retraction_speed))            
            if(is_extrude_only_code(line) and current_layer >= base_height / layer_height):
                    new_code = adjust_extrude_speed(line, current_retraction_speed * 60) #convert to mm/m
                    output_file.write(str(new_code))
            else:
                output_file.write(line)

def generate_floor_layers():
    layer_numbers = []
    current_floor = 0
    while current_floor < num_floors:
        layer_num = round(current_floor * floor_layers + base_layers)
        layer_numbers.append(layer_num)
        current_floor += 1
    print(layer_numbers)
    layer_numbers.pop(0)
    return layer_numbers

def is_movement_code(gcode):
    return bool(re.search(r"^G1.+X.+Y.+E(\-*\d+\.*\d*)", gcode))

def is_extrude_only_code(gcode):
    return bool(re.search(r"^G1\s+E(\-*\d+\.*\d*)",gcode))

def is_default_extrude(gcode):
    p = re.compile(r"E(?P<extrude_amount>\-*\d+\.*\d*)")
    specified_amount = p.search(gcode).group("extrude_amount")
    if(float(specified_amount) == input_file_retraction_setting):
        return True
    return False
    
# G1 E1.00000 F2400.00000
# G1 E-1.00000 F2400.00000
# G1 X2.614 Y-4.728 E1.0790
def adjust_extrude_amount(gcode, offset):
    p = re.compile(r"E(?P<extrude_amount>\-*\d+\.*\d*)")
    current_amount = (p.search(gcode).group("extrude_amount"))
    new_amount = float(current_amount) + offset
    if new_amount == 0:
        return ''
    new_amount = "{:.5f}".format(new_amount) # Set to 5 decimals
    replacement = p.sub('E' + new_amount,gcode)
    return replacement

def adjust_extrude_speed(gcode, new_amount):
    p = re.compile(r"F(?P<extrude_speed>\-*\d+\.*\d*)")
    if(new_amount != 0):
        new_amount = "{:.5f}".format(new_amount) # Set to 5 decimals
        replacement = p.sub('F' + new_amount, gcode)
    else:
        new_amount = "{:.5f}".format(60)
        replacement =  p.sub('F' + new_amount, gcode) # 1mm /s
    return replacement


# gcode = "G1 X2.614 Y-4.728 E1.0790"
# gcode = "G1 E-5.00000 F2400.00000"
# gcode = "G1 E1.00000 F2400.00000"
# gcode = "G1 E2.00000 F2400.00000"
# gcode = "G1 EA"
# print(is_extrude_code(gcode))
# result = is_default_extrude(gcode)
# print(result)

main()
# adjust_ret_speed()
# print(generate_floor_layers())
