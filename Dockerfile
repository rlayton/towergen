FROM ubuntu:disco

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y
RUN apt-get install vim python openscad slic3r -y